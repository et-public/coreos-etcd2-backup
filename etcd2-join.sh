#!/bin/bash

if [ -f /etc/profile.d/etcdctl.sh ];
then
  source /etc/profile.d/etcdctl.sh
  etcdctl cluster-health
  echo "Do you want to contiune? [Y/N]: "; read ANSWER
  if [ ! "$ANSWER" = "Y" ]; then
    echo "Not to join a new cluster."
    exit 1
  fi
fi

if [ ! -f /etc/etcd2-backup-enabled ];
then
  TIMESTAMP=$(date +%Y-%m-%H-%M)
  sudo systemctl stop etcd2 etcd2-backup
  if systemctl is-enabled etcd2-backup ;
  then
    sudo systemctl stop etcd2-backup
  fi
  [ -d /var/lib/etcd2/member ] && sudo mv -v /var/lib/etcd2/member /var/lib/etcd2/member.${TIMESTAMP}
  [ -d /etc/sysconfig ] && sudo mv -v /etc/sysconfig/ /etc/sysconfig.${TIMESTAMP}
  /opt/bin/etcd-init.sh && sleep 5 && sudo systemctl start etcd2.service
  etcdctl cluster-health
else
  echo "A backup master. Should not run join."
fi