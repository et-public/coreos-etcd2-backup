##  CoreOS etcd2 backup and restores

You only need to run this on one of the etcd2 members. This machine will be the one you can restore
data and form a signle node cluster; the other etcd members will re-join to the new cluster.

Adapted from: [etcd2-backup-coreos](https://github.com/coreos/etcd/tree/master/contrib/systemd/etcd2-backup-coreos)

### Install etcd2 backup and restore files

Install this on all etcd members:

```console
$ cd /home/core && rm -rf coreos-etcd2-backup
$ git clone https://code.stanford.edu/et-public/coreos-etcd2-backup.git
$ cd coreos-etcd2-backup
```
### Setup the backup

Only need to do this on one of the 3 etcd2 machines to setup backup. It doesn't hurt to run backups
on all of them though. The path to the backup will have the machine id as prefix, so it won't step into
each others backup.

Before you run the `setup.sh`, the backup bucket has to exist, and the etcd2 instance role has the IAM policy
to read/rewrite the bucket. The general configuration involves setting the following two environment variables:

```console
$ export PATHTOBACKUP=>bucket/path-to-backup-folder
$ export RCLONE_CONFIG=<region>
```

For specific cluster:

* itlab-us-west

```console
$ export RCLONE_CONFIG=us-west-2
$ export PATHTOBACKUP=itlab-backups/etcd2-backups
```
* anchorage-us-west

```console
$ export RCLONE_CONFIG=us-west-2
$ export PATHTOBACKUP=anchorage-backups/etcd2-backups
```

Then, run `setup.sh` on each machine to install files, but only answer "Y" if you want to enable the etcd data backup:

```console
$ pwd
/home/core/core-os-etcd2-backups
$ ./setup.sh
```

To verify:

```console
$ systemctl status etcd2-backup
```

### Restore

Stop etcd2 services on all etcd members

```console
$ sudo systemctl stop etcd2
```

On the machine that you backup your data:

```console
$ pwd
/home/core/core-os-etcd2-backups
$ ./etcd2-restore.sh
```

### Join cluster

On other two etcd2 members (not the one you run restore)

```console
$ cd /home/core && rm -rf coreos-etcd2-backup
$ git clone https://code.stanford.edu/et-public/coreos-etcd2-backup.git
$ cd coreos-etcd2-backup
$ ./etcd2-join.sh
```

### Reconfigure the backup and restore files

You need to build the package on your local machine with `go` installed.
Most likely, you need to configure `rclone.conf` and `30etcd2-backup-restore.conf` file.
After modification of these files:

```console
$ git clone https://code.stanford.edu/et-public/coreos-etcd2-backup.git
$ cd coreos-etcd2-backups
$ ./build
$ git add .
$ git commit -m "commit message"
$ git push
```
