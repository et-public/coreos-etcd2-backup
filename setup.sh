#!/bin/bash -e

abort(){
  echo $* && exit 1
}
. /etc/os-release
if [ ! "$ID" == "coreos" ];then
    abort "os-release error: Detected ID=$ID: this is not CoreOS Linux"
fi

# Define backup location for environment variable.
PATHTOBACKUP=${PATHTOBACKUP:-}
RCLONE_CONFIG=${RCLONE_CONFIG:-}
if [[ -z ${PATHTOBACKUP} ]] ||  [[ -z ${RCLONE_CONFIG} ]]; then
  abort "Need to export PATHTOBACKUP and RCLONE_CONFIG"
fi

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -f ${SCRIPT_DIR}/rclone.conf ];then
    abort "Could not find ${SCRIPT_DIR}/rclone.conf."
else
  echo Copy ${SCRIPT_DIR}/rclone.conf to /etc/rclone.conf
  sudo cp -f ${SCRIPT_DIR}/rclone.conf  /etc/rclone.conf
fi

sudo mkdir -p /opt/bin
sudo cp -vf etcd2-join /opt/bin
sudo cp -vf bin/etcd2-restore /opt/bin
sudo cp -vf ${SCRIPT_DIR}/*.{service,timer} /etc/systemd/system
echo "Reload systemd"
sudo systemctl daemon-reload

for jobtype in restore backup join; do
     sudo mkdir -p /var/run/systemd/system/etcd2-${jobtype}.service.d
     cat 30-etcd2-backup-restore.conf | \
	     envsubst > /tmp/30-etcd2-backup-restore.conf
     echo "cp -rvf /tmp/30-etcd2-backup-restore.conf /var/run/systemd/system/etcd2-${jobtype}.service.d/"
     sudo cp -rvf /tmp/30-etcd2-backup-restore.conf /var/run/systemd/system/etcd2-${jobtype}.service.d/
     echo "ln -vsf /var/run/systemd/system/etcd2{,-${jobtype}}.service.d/20-cloudinit.conf"
     sudo ln -vsf /var/run/systemd/system/etcd2{,-${jobtype}}.service.d/20-cloudinit.conf
done
if sudo systemctl is-enabled etcd2-backup ;
then
  sudo systemctl stop etcd2-backup
fi
sudo systemctl daemon-reload

echo "etcd2-backup install complete!"
echo "Do you want to enable backup on this node? [Y/N]: "; read ANSWER
if [ ! "$ANSWER" = "Y" ]; then
    abort "Backup is not enabled."
else
    sudo touch /etc/etcd2-backup-enabled
    sudo systemctl enable etcd2-backup.timer
    sudo systemctl start etcd2-backup.timer
fi
