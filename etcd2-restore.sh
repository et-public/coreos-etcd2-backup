#!/bin/bash
if [ -f /etc/profile.d/etcdctl.sh ];
then
  source /etc/profile.d/etcdctl.sh
fi
if [ -f /etc/etcd2-backup-enabled ];
then
  echo "Restore to a new cluster. Do you want to contiune? [Y/N]: "; read ANSWER
  if [ ! "$ANSWER" = "Y" ]; then
    echo "Remember to stop etcd2 services on all etcd members."
    echo "Not to restore to a new cluster."
    exit 1
  fi
  sudo systemctl stop etcd2 etcd2-backup.timer etcd2-backup
  sudo systemctl start etcd2-restore &&  sudo systemctl start etcd2 && sleep 5
  etcdctl cluster-health
  echo "If the cluster is health, you can go ahead to run etcd2-join on other etcd members."
  echo "Don't forget to restart the backup when the cluster is fully recovered."
else
  echo "No backup on from this machine. Nothing to do."
  exit 1
fi